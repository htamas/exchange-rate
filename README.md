# Currency Exchange

A simple Spring Boot API that serves the conversion rate of currencies according to the ECB references rates. 
The service checks ECB's exchange rates periodically in an asynchronous job back to 90 days, and stores them in memory.

## Compilation

In order to compile the application you need to have **JDK8** and **Maven** installed.

To compile and package the project, execute the unit tests and verify the code coverage use the following command:

`mvn clean verify`

**Note**: The project is using Lombok for code generation of POJOs. 
If you are building it in an IDE, you are going to need a Lombok extension installed.

## Usage

The build produces a self-contained jar file which you can use to start the application with the following command:

`java -jar -Dspring.config.location=[configfile/location/] exchange/target/exchange-0.1.0.jartarget/exchange-0.1.0.jar`

Specifying the config file location is optional if you are running the command from that location.

To access the API endpoint use the following url: [http://localhost:8080/](http://localhost:8080/)
The API currently has one endpoint: `/api/v1/rate/?currency{currency}&date={date}`

### Examples

Exchange an amount with the most recent rates: 
[localhost:8080/api/v1/rates/USD](localhost:8080/api/v1/rates/USD)

Exchange an amount with a rate in the past (up to 90 days):
[localhost:8080/api/v1/rates/USD?date=2018-10-01](localhost:8080/api/v1/rates/USD?date=2018-10-01)
If the date is not available on ECB (due to being a bank holiday) it tries to find the previous available day.

## Configuration

The application configuration is in YAML format, in `application.yml`.

* `ecb90DayHistoryUrl`: the URL of the ECB's 90 day history of reference currency rates

### Variables
Environment variable|Description|Default
|:-------------------|:--------------------|:--------------------|
|`ECB_90_DAY_HISTORY_URL`|Sets the config property `ecb90DayHistoryUrl`|http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml|
|`ECB_REFRESH_MS`|Sets the refresh rate of the ECB currency rate cache in milliseconds|600000 (10 minutes)|

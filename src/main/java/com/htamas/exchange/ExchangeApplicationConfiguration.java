package com.htamas.exchange;

import com.htamas.exchange.client.CurrencyRateProvider;
import com.htamas.exchange.client.ECBClient;
import com.htamas.exchange.job.ExchangeRateDataRefresherJob;
import com.htamas.exchange.repository.ExchangeRateRepository;
import com.htamas.exchange.repository.InMemoryExchangeRateRepository;
import com.htamas.exchange.service.ExchangeRateService;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.policy.CircuitBreakerRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableScheduling
@EnableAsync
@EnableRetry
public class ExchangeApplicationConfiguration {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public RetryTemplate retryTemplate() {
        RetryTemplate template = new RetryTemplate();
        CircuitBreakerRetryPolicy retryPolicy = new CircuitBreakerRetryPolicy();
        retryPolicy.setOpenTimeout(5000L);
        retryPolicy.setResetTimeout(1000L);
        template.setRetryPolicy(retryPolicy);
        return template;
    }

    @Bean
    public ExchangeRateService exchangeRateService(ExchangeRateRepository exchangeRateRepository) {
        return new ExchangeRateService(exchangeRateRepository);
    }

    @Bean
    public ExchangeRateRepository exchangeRateRepository() {
        return new InMemoryExchangeRateRepository();
    }

    @Bean
    public CurrencyRateProvider currencyRateProvider(RestTemplate restTemplate, RetryTemplate retryTemplate) {
        return new ECBClient(restTemplate, retryTemplate);
    }

    @Bean
    public ExchangeRateDataRefresherJob exchangeRateDataRefresherJob(CurrencyRateProvider currencyRateProvider,
                                                                     ExchangeRateRepository exchangeRateRepository) {
        return new ExchangeRateDataRefresherJob(currencyRateProvider, exchangeRateRepository);
    }
}

package com.htamas.exchange.repository;

import com.htamas.exchange.core.ExchangeRate;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class InMemoryExchangeRateRepository implements ExchangeRateRepository {

    private AtomicReference<ConcurrentHashMap<LocalDate, Set<ExchangeRate>>> historicalExchangeRatesReference =
            new AtomicReference<>(new ConcurrentHashMap<>());

    @Override
    public Optional<ExchangeRate> getExchangeRateOnDate(LocalDate date, String currency) {
        Set<ExchangeRate> rates = findMostRecentRateOfDate(date);
        if (rates.isEmpty()) {
            return Optional.empty();
        }

        return rates.stream()
                .filter(rate -> rate.getCurrency().equals(currency))
                .findFirst();
    }

    @Override
    public void setHistoricalExchangeRates(Map<LocalDate, Set<ExchangeRate>> exchangeRatesByDate) {
        historicalExchangeRatesReference.set(new ConcurrentHashMap<>(exchangeRatesByDate));
        log.info("Historical exchange rate data update finished with {} entries.", exchangeRatesByDate.size());
    }

    private Set<ExchangeRate> findMostRecentRateOfDate(LocalDate localDate) {
        for (int i = 0; i < 7; i++) {
            LocalDate pastDate = localDate.minusDays(i);
            Set<ExchangeRate> currencyRates = historicalExchangeRatesReference.get().get(pastDate);
            if (currencyRates != null) {
                return currencyRates;
            }
        }

        return Collections.emptySet();
    }

}

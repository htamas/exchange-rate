package com.htamas.exchange.repository;

import com.htamas.exchange.core.ExchangeRate;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public interface ExchangeRateRepository {

    Optional<ExchangeRate> getExchangeRateOnDate(LocalDate date, String currency);

    void setHistoricalExchangeRates(Map<LocalDate, Set<ExchangeRate>> exchangeRatesByDate);
}

package com.htamas.exchange.job;

import com.htamas.exchange.client.CurrencyRateProvider;
import com.htamas.exchange.core.ExchangeRate;
import com.htamas.exchange.repository.ExchangeRateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

@Slf4j
public class ExchangeRateDataRefresherJob {

    private CurrencyRateProvider currencyRateProvider;
    private ExchangeRateRepository exchangeRateRepository;

    public ExchangeRateDataRefresherJob(CurrencyRateProvider currencyRateProvider,
                                        ExchangeRateRepository exchangeRateRepository) {
        this.currencyRateProvider = currencyRateProvider;
        this.exchangeRateRepository = exchangeRateRepository;
    }

    @PostConstruct
    public void init() {
        this.updateHistoricalRates();
    }

    @Scheduled(fixedRateString = "${ECB_REFRESH_MS:600000}")
    public void updateHistoricalRates() {
        log.info("Refreshing historical exchange rates");
        Map<LocalDate, Set<ExchangeRate>> newRates;
        try {
            newRates = currencyRateProvider.getExchangeRatesOfDates();
        } catch (Exception e) {
            log.error("Error communicating with currency rate provider, cannot update exchange rates", e);
            return;
        }
        if (newRates == null || newRates.isEmpty()) {
            log.error("Couldn't obtain new data, keeping entries in place this time");
            return;
        }

        exchangeRateRepository.setHistoricalExchangeRates(newRates);
    }
}

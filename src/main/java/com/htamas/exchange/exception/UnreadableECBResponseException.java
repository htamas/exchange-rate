package com.htamas.exchange.exception;

public class UnreadableECBResponseException extends RuntimeException {
    public UnreadableECBResponseException(String message) {
        super(message);
    }
}

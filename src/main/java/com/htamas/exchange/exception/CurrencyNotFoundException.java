package com.htamas.exchange.exception;

import java.util.Objects;

public class CurrencyNotFoundException extends RuntimeException {
    private String currency;

    public CurrencyNotFoundException(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CurrencyNotFoundException)) return false;
        CurrencyNotFoundException that = (CurrencyNotFoundException) o;
        return Objects.equals(currency, that.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currency);
    }
}

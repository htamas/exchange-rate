package com.htamas.exchange.service;

import com.htamas.exchange.core.ExchangeRate;
import com.htamas.exchange.exception.CurrencyNotFoundException;
import com.htamas.exchange.repository.ExchangeRateRepository;

import java.time.LocalDate;

public class ExchangeRateService {

    private ExchangeRateRepository repository;

    public ExchangeRateService(ExchangeRateRepository repository) {
        this.repository = repository;
    }

    public ExchangeRate findRate(String currency, LocalDate date) {
        return repository.getExchangeRateOnDate(date, currency)
                .orElseThrow(() -> new CurrencyNotFoundException(currency));
    }
}

package com.htamas.exchange.controller;

import com.htamas.exchange.api.ErrorResponse;
import com.htamas.exchange.exception.CurrencyNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExchangeApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({CurrencyNotFoundException.class})
    protected ResponseEntity handleCurrencyNotFound(CurrencyNotFoundException ex, WebRequest request) {
        ErrorResponse response = new ErrorResponse("Not Found", "Currency not found: " + ex.getCurrency());
        return handleExceptionInternal(ex, response,
                new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

}

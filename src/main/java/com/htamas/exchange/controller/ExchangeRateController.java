package com.htamas.exchange.controller;

import com.htamas.exchange.api.ExchangeResponse;
import com.htamas.exchange.core.ExchangeRate;
import com.htamas.exchange.service.ExchangeRateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.time.ZoneId;

@Controller
public class ExchangeRateController {

    private String timeZone;
    private ExchangeRateService exchangeRateService;

    public ExchangeRateController(@Value("${timeZone}") String timeZone, ExchangeRateService exchangeRateService) {
        this.timeZone = timeZone;
        this.exchangeRateService = exchangeRateService;
    }

    @ResponseBody
    @RequestMapping("/api/v1/rates/{currency}")
    public ExchangeResponse getRate(@PathVariable(name = "currency") String currency,
                                    @RequestParam(name = "date", required = false)
                                    @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        LocalDate today = LocalDate.now(ZoneId.of(timeZone));
        if (date == null) {
            date = today;
        }

        currency = currency.toUpperCase();

        ExchangeRate rate = exchangeRateService.findRate(currency, date);

        return ExchangeResponse.builder()
                .currency(rate.getCurrency())
                .rate(rate.getRate())
                .build();
    }
}

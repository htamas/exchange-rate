package com.htamas.exchange.client;

import com.htamas.exchange.core.ExchangeRate;
import com.htamas.exchange.core.ecb.Envelope;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

public class ECBClient implements CurrencyRateProvider {

    private RestTemplate restTemplate;
    private RetryTemplate retryTemplate;

    @Value("${ecb90DayHistoryUrl}")
    private String ecb90DayHistoryUrl;

    public ECBClient(RestTemplate restTemplate, RetryTemplate retryTemplate) {
        this.restTemplate = restTemplate;
        this.retryTemplate = retryTemplate;
    }

    @Override
    public Map<LocalDate, Set<ExchangeRate>> getExchangeRatesOfDates() {
        Envelope envelope = retryTemplate.execute(cb -> restTemplate.getForEntity(
                ecb90DayHistoryUrl,
                Envelope.class).getBody());

        return envelope.asMap();
    }
}

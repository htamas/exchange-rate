package com.htamas.exchange.client;

import com.htamas.exchange.core.ExchangeRate;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

public interface CurrencyRateProvider {

    Map<LocalDate, Set<ExchangeRate>> getExchangeRatesOfDates();

}

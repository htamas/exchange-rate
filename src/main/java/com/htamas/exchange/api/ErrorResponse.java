package com.htamas.exchange.api;

import lombok.Value;

@Value
public class ErrorResponse {
    private String error;
    private String reason;
}

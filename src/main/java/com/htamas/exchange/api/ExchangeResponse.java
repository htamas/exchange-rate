package com.htamas.exchange.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class ExchangeResponse {
    private String currency;
    private String rate;
}

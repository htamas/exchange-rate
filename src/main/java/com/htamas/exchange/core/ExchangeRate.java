package com.htamas.exchange.core;


import lombok.Value;

@Value
public class ExchangeRate {
    private final String currency;
    private final String rate;
}

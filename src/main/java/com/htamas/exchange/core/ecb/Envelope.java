package com.htamas.exchange.core.ecb;

import com.htamas.exchange.core.ExchangeRate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@XmlRootElement(name = "Envelope", namespace = "http://www.gesmes.org/xml/2002-08-01")
@XmlType(propOrder = {"subject", "sender", "cube"})
@XmlAccessorType(XmlAccessType.NONE)
public class Envelope {

    private static final Logger LOGGER = LoggerFactory.getLogger(Envelope.class);

    private String subject;
    private Sender sender;
    private Cube cube;

    public Envelope() {
    }

    public Envelope(String subject, Sender sender, Cube cube) {
        this.subject = subject;
        this.sender = sender;
        this.cube = cube;
    }

    public Map<LocalDate, Set<ExchangeRate>> asMap() {
        List<ECBCurrencyRatesOfDate> currencyRatesOfDateList = this.getCube().getECBCurrencyRatesOfDateList();
        return currencyRatesOfDateList.stream()
                .collect(Collectors.toMap(
                        ratesOfDate -> {
                            String date = ratesOfDate.getDate();
                            return LocalDate.parse(date, DateTimeFormatter.ISO_DATE);
                        },
                        ratesOfDate -> ratesOfDate.getECBCurrencyRates().stream()
                                .map(rate -> new ExchangeRate(rate.getCurrency(), rate.getRate()))
                                .collect(Collectors.toSet())
                ));
    }

    @XmlElement
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @XmlElement(name = "Sender")
    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    @XmlElement(name = "Cube", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
    public Cube getCube() {
        return cube;
    }

    public void setCube(Cube cube) {
        this.cube = cube;
    }
}

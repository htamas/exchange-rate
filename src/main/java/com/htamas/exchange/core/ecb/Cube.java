package com.htamas.exchange.core.ecb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlType(namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
@XmlAccessorType(XmlAccessType.NONE)
public class Cube {

    private List<ECBCurrencyRatesOfDate> ECBCurrencyRatesOfDateList;

    public Cube() {
    }

    public Cube(List<ECBCurrencyRatesOfDate> ECBCurrencyRatesOfDateList) {
        this.ECBCurrencyRatesOfDateList = ECBCurrencyRatesOfDateList;
    }

    @XmlElement(name = "Cube")
    public List<ECBCurrencyRatesOfDate> getECBCurrencyRatesOfDateList() {
        return ECBCurrencyRatesOfDateList;
    }

    public void setECBCurrencyRatesOfDateList(List<ECBCurrencyRatesOfDate> ECBCurrencyRatesOfDateList) {
        this.ECBCurrencyRatesOfDateList = ECBCurrencyRatesOfDateList;
    }
}

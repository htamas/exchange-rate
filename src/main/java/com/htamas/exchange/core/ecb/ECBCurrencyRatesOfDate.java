package com.htamas.exchange.core.ecb;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlType(namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
@XmlAccessorType(XmlAccessType.NONE)
public class ECBCurrencyRatesOfDate {

    private String date;
    private List<ECBCurrencyRate> ECBCurrencyRates;

    public ECBCurrencyRatesOfDate() {
    }

    public ECBCurrencyRatesOfDate(String date, List<ECBCurrencyRate> ECBCurrencyRates) {
        this.date = date;
        this.ECBCurrencyRates = ECBCurrencyRates;
    }

    @XmlAttribute(name = "time")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @XmlElement(name = "Cube")
    public List<ECBCurrencyRate> getECBCurrencyRates() {
        return ECBCurrencyRates;
    }

    public void setECBCurrencyRates(List<ECBCurrencyRate> ECBCurrencyRates) {
        this.ECBCurrencyRates = ECBCurrencyRates;
    }
}

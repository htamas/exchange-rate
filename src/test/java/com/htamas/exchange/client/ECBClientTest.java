package com.htamas.exchange.client;

import com.htamas.exchange.core.ExchangeRate;
import com.htamas.exchange.core.ecb.Cube;
import com.htamas.exchange.core.ecb.ECBCurrencyRate;
import com.htamas.exchange.core.ecb.ECBCurrencyRatesOfDate;
import com.htamas.exchange.core.ecb.Envelope;
import com.htamas.exchange.core.ecb.Sender;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ECBClientTest {

    private static final String CURRENCY = "USD";
    private static final String RATE = "1.1";
    private static final String DATE = "2017-05-26";
    private static final ECBCurrencyRate ECB_CURRENCY_RATE = new ECBCurrencyRate(CURRENCY, RATE);
    private static final ECBCurrencyRatesOfDate ECB_CURRENCY_RATES_OF_DATE =
            new ECBCurrencyRatesOfDate(DATE, Lists.newArrayList(ECB_CURRENCY_RATE));
    private static final Cube CUBE = new Cube(Lists.newArrayList(ECB_CURRENCY_RATES_OF_DATE));
    private static final Envelope ENVELOPE =
            new Envelope("Reference rates", new Sender("European Central Bank"), CUBE);

    private RestTemplate restTemplate = mock(RestTemplate.class);
    private RetryTemplate retryTemplate = new RetryTemplate();
    private ECBClient client = new ECBClient(restTemplate, retryTemplate);

    @Test
    public void getExchangeRatesOfDates() {
        ResponseEntity<Envelope> responseEntity = new ResponseEntity<>(ENVELOPE, HttpStatus.OK);

        when(restTemplate.getForEntity(anyString(), eq(Envelope.class))).thenReturn(responseEntity);

        Map<LocalDate, Set<ExchangeRate>> exchangeRatesOfDates = client.getExchangeRatesOfDates();

        LocalDate expectedDate = LocalDate.parse(DATE);

        ExchangeRate expectedExchangeRate = new ExchangeRate(CURRENCY, RATE);
        assertThat(exchangeRatesOfDates).containsKey(expectedDate);
        Set<ExchangeRate> exchangeRates = exchangeRatesOfDates.get(expectedDate);
        assertThat(exchangeRates).containsExactly(expectedExchangeRate);
    }

}

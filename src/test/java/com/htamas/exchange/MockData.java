package com.htamas.exchange;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.htamas.exchange.core.ExchangeRate;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public interface MockData {
    LocalDate SOME_DATE = LocalDate.parse("2018-01-01");
    String USD = "USD";
    ExchangeRate USD_EXCHANGE_RATE = new ExchangeRate(USD, "1.1193");
    HashSet<ExchangeRate> EXCHANGE_RATES = Sets.newHashSet(USD_EXCHANGE_RATE);
    Map<LocalDate, Set<ExchangeRate>> HISTORICAL_EXCHANGE_RATES =
            ImmutableMap.of(SOME_DATE, EXCHANGE_RATES);
}

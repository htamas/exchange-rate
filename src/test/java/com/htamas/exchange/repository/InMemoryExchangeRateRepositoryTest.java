package com.htamas.exchange.repository;

import com.htamas.exchange.core.ExchangeRate;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static com.htamas.exchange.MockData.HISTORICAL_EXCHANGE_RATES;
import static com.htamas.exchange.MockData.SOME_DATE;
import static com.htamas.exchange.MockData.USD;
import static com.htamas.exchange.MockData.USD_EXCHANGE_RATE;

public class InMemoryExchangeRateRepositoryTest {

    private InMemoryExchangeRateRepository repository = new InMemoryExchangeRateRepository();

    @Test
    public void getExchangeRateOnDate() {
        Optional<ExchangeRate> exchangeRateOnDate = repository.getExchangeRateOnDate(SOME_DATE, USD);
        assertThat(exchangeRateOnDate.isPresent()).isFalse();

        repository.setHistoricalExchangeRates(HISTORICAL_EXCHANGE_RATES);
        exchangeRateOnDate = repository.getExchangeRateOnDate(SOME_DATE, USD);

        assertThat(exchangeRateOnDate.isPresent()).isTrue();
        assertThat(exchangeRateOnDate.get()).isEqualTo(USD_EXCHANGE_RATE);
    }

    @Test
    public void getExchangeRateOnDateCheckPreviousDays() {
        repository.setHistoricalExchangeRates(HISTORICAL_EXCHANGE_RATES);
        LocalDate nextDay = SOME_DATE.plusDays(1);
        Optional<ExchangeRate> exchangeRateOnDate = repository.getExchangeRateOnDate(nextDay, USD);

        assertThat(exchangeRateOnDate.isPresent()).isTrue();
        assertThat(exchangeRateOnDate.get()).isEqualTo(USD_EXCHANGE_RATE);
    }

    @Test
    public void getExchangeRateOnDateReturnsEmptyIfNoDateFound() {
        Optional<ExchangeRate> exchangeRateOnDate = repository.getExchangeRateOnDate(SOME_DATE, USD);
        assertThat(exchangeRateOnDate.isPresent()).isFalse();

        repository.setHistoricalExchangeRates(HISTORICAL_EXCHANGE_RATES);
        exchangeRateOnDate = repository.getExchangeRateOnDate(LocalDate.parse("2000-01-01"), USD);

        assertThat(exchangeRateOnDate.isPresent()).isFalse();
    }

    @Test
    public void getExchangeRateOnDateReturnsEmptyIfNoCurrencyFound() {
        Optional<ExchangeRate> exchangeRateOnDate = repository.getExchangeRateOnDate(SOME_DATE, USD);
        assertThat(exchangeRateOnDate.isPresent()).isFalse();

        repository.setHistoricalExchangeRates(HISTORICAL_EXCHANGE_RATES);
        exchangeRateOnDate = repository.getExchangeRateOnDate(SOME_DATE, "HUF");

        assertThat(exchangeRateOnDate.isPresent()).isFalse();
    }
}

package com.htamas.exchange.job;

import com.htamas.exchange.MockData;
import com.htamas.exchange.client.ECBClient;
import com.htamas.exchange.core.ExchangeRate;
import com.htamas.exchange.repository.ExchangeRateRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class ExchangeRateDataRefresherJobTest {

    @Captor
    private ArgumentCaptor<Map<LocalDate, Set<ExchangeRate>>> exchangeRateMapCaptor;

    @Mock
    private ECBClient mockEcbClient;

    @Mock
    private ExchangeRateRepository mockRepository;

    private ExchangeRateDataRefresherJob job;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        job = new ExchangeRateDataRefresherJob(mockEcbClient, mockRepository);
    }

    @Test
    public void updateHistoricalRates() {
        when(mockEcbClient.getExchangeRatesOfDates()).thenReturn(MockData.HISTORICAL_EXCHANGE_RATES);

        job.updateHistoricalRates();

        verify(mockRepository).setHistoricalExchangeRates(exchangeRateMapCaptor.capture());
        Map<LocalDate, Set<ExchangeRate>> exchangeRatesMap = exchangeRateMapCaptor.getValue();
        assertThat(exchangeRatesMap).isEqualTo(MockData.HISTORICAL_EXCHANGE_RATES);
    }

    @Test
    public void updateHistoricalExchangeRatesDontUpdateOnException() {
        when(mockEcbClient.getExchangeRatesOfDates()).thenThrow(new RuntimeException());

        job.updateHistoricalRates();

        verifyZeroInteractions(mockRepository);
    }

    @Test
    public void updateHistoricalExchangeRatesDontUpdateOnEmptyResponse() {
        when(mockEcbClient.getExchangeRatesOfDates()).thenReturn(new HashMap<>());

        job.updateHistoricalRates();

        verifyZeroInteractions(mockRepository);

        when(mockEcbClient.getExchangeRatesOfDates()).thenReturn(null);

        job.updateHistoricalRates();

        verifyZeroInteractions(mockRepository);
    }
}

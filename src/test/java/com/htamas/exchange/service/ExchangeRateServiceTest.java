package com.htamas.exchange.service;

import com.htamas.exchange.core.ExchangeRate;
import com.htamas.exchange.repository.ExchangeRateRepository;
import org.junit.Test;

import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static com.htamas.exchange.MockData.SOME_DATE;
import static com.htamas.exchange.MockData.USD;
import static com.htamas.exchange.MockData.USD_EXCHANGE_RATE;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExchangeRateServiceTest {

    private ExchangeRateRepository mockRepository = mock(ExchangeRateRepository.class);
    private ExchangeRateService exchangeRateService = new ExchangeRateService(mockRepository);

    @Test
    public void findRateReturnsRatesOfADate() {
        when(mockRepository.getExchangeRateOnDate(SOME_DATE, USD)).thenReturn(Optional.of(USD_EXCHANGE_RATE));

        ExchangeRate rate = exchangeRateService.findRate(USD, SOME_DATE);

        assertThat(rate).isEqualTo(USD_EXCHANGE_RATE);
    }
}

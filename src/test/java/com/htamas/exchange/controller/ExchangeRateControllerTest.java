package com.htamas.exchange.controller;

import com.htamas.exchange.api.ExchangeResponse;
import com.htamas.exchange.core.ExchangeRate;
import com.htamas.exchange.service.ExchangeRateService;
import org.junit.Test;

import java.time.LocalDate;
import java.time.ZoneId;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExchangeRateControllerTest {

    private static final String USD = "USD";
    private static final String TIME_ZONE = "CET";
    private ExchangeRateService exchangeRateService = mock(ExchangeRateService.class);
    private ExchangeRateController controller = new ExchangeRateController(TIME_ZONE, exchangeRateService);

    @Test
    public void getRate() {
        String rate = "1.1576";
        LocalDate today = LocalDate.now(ZoneId.of(TIME_ZONE));
        ExchangeRate currentRate = new ExchangeRate(USD, rate);
        when(exchangeRateService.findRate(USD, today)).thenReturn(currentRate);

        ExchangeResponse response = controller.getRate(USD, null);

        ExchangeResponse expectedResponse = new ExchangeResponse(USD, rate);
        assertThat(response).isEqualTo(expectedResponse);
    }

    @Test
    public void getRateConvertsCurrencyToUppercase() {
        String rate = "1.1576";
        LocalDate today = LocalDate.now(ZoneId.of(TIME_ZONE));
        ExchangeRate currentRate = new ExchangeRate(USD, rate);
        when(exchangeRateService.findRate(USD, today)).thenReturn(currentRate);

        ExchangeResponse response = controller.getRate(USD.toLowerCase(), null);

        ExchangeResponse expectedResponse = new ExchangeResponse(USD, rate);
        assertThat(response).isEqualTo(expectedResponse);
    }

    @Test
    public void getRateOfPast() {
        LocalDate someDate = LocalDate.parse("2018-09-01");
        String rate = "1.11";
        ExchangeRate pastRate = new ExchangeRate(USD, rate);
        when(exchangeRateService.findRate(USD, someDate)).thenReturn(pastRate);

        ExchangeResponse response = controller.getRate(USD, someDate);

        ExchangeResponse expectedResponse = new ExchangeResponse(USD, rate);
        assertThat(response).isEqualTo(expectedResponse);
    }
}

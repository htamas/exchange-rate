package com.htamas.exchange.controller;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.yml")
@AutoConfigureMockMvc
public class ExchangeRateControllerIT {

    // Set up mock remote service before background jobs start
    @Order(0)
    @TestConfiguration
    static class TestConfig {
        @Bean
        public WireMockServer wiremockServer() {
            WireMockServer wireMockServer = new WireMockServer(options().port(8088));
            wireMockServer.stubFor(com.github.tomakehurst.wiremock.client.WireMock.get(urlEqualTo("/ecbrates"))
                    .willReturn(aResponse()
                            .withHeader("Content-Type", "text/xml")
                            .withBodyFile("ecb90Response.xml")));

            wireMockServer.start();
            return wireMockServer;
        }
    }

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    WireMockServer wireMockServer;

    @After
    public void teardown() {
        wireMockServer.stop();
    }

    @Test
    public void testGetRateWithoutSpecifiedDate() throws Exception {
        mockMvc.perform(get("/api/v1/rates/USD"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.currency").value("USD"))
                .andExpect(jsonPath("$.rate").value("1.1506"));
    }

    @Test
    public void testGetRateWithSpecifiedDate() throws Exception {
        mockMvc.perform(get("/api/v1/rates/USD")
                .param("date", "2018-10-02"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.currency").value("USD"))
                .andExpect(jsonPath("$.rate").value("1.1543"));
    }
}
